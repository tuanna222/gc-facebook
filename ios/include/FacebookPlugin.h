#import "PluginManager.h"
#import <FacebookSDK/FacebookSDK.h>

@interface FacebookPlugin : GCPlugin

@property (retain, nonatomic) UINavigationController *navController;
@property (nonatomic, retain) TeaLeafViewController *tealeafViewController;
@property (nonatomic, retain) TeaLeafAppDelegate *teaLeafAppDelegate;

- (void) sessionStateChanged:(FBSession *)session
					   state:(FBSessionState) state
					   error:(NSError *)error;

- (void) openSession:(BOOL)allowLoginUI;

@end
